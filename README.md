[![Mircoin logo](https://mircoin.community/img/logo.png)](https://mircoin.community)

# Mircoin Community #

[Mircoin](https://mircoin.community) is community-based cryptocurrency.

### Features

MRC inherits features from NXT cryptocurrency.

The most important features that we love are:

* Proof of Stake algorithm
* Encrypted messages
* Monetary system
* Dividends and Assets with built-in exchange
* Advanced voting module
* Marketplace
* Phased transactions
* Shuffling


### Distribution

We contribute to community members for their activity.
[Here](https://mircoin.community/en/awards) you can check current awards list.

### Nodes reward program

Run your own node and register it [here](https://mircoin.community/en/nodes) to earn MRC every hour.

